package com.company;

import java.util.Scanner;

class Exception {

    public static void main(String[] args) {
        // prompt the user to enter input
        Scanner input = new Scanner(System.in);
        System.out.print("Input the first number (numerator): ");
        int num1 = input.nextInt();
        System.out.print("Input the second number (denominator): ");
        int num2 = input.nextInt();
        System.out.println();
        int result = num1/num2;
        System.out.println("Division result of " + num1 + "/" + num2 + "= " + result);
}
        }
